package com.gitlab.edufuga.wordlines.vocabularycleaner

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.text.SimpleDateFormat
import java.util.stream.Collectors
import java.util.stream.Stream

class VocabularyCleaner {
    private Path root

    VocabularyCleaner(Path root) {
        this.root = root
    }

    void clean() {
        println "I'm cleaning. Stay tuned."

        List<Path> allPaths = filewalk(root)

        List<WordStatusDate> allRecords = new ArrayList<>()
        for (Path path : allPaths) {
            WordStatusDate record = readRecord(path)
            allRecords.add(record)
        }

        println "Number of records: " + allRecords.size()
        println ""

        Map<String, List<WordStatusDate>> recordsByLoweredCaseWord = new HashMap<>()
        for (WordStatusDate record : allRecords) {
            String loweredCasedWord = record.getWord().toLowerCase()
            if (loweredCasedWord == null || loweredCasedWord.isEmpty()) {
                continue
            }
            recordsByLoweredCaseWord.putIfAbsent(loweredCasedWord, new ArrayList<WordStatusDate>())
            recordsByLoweredCaseWord.get(loweredCasedWord).add(record)
        }

        for (Map.Entry<String, List<WordStatusDate>> entry : recordsByLoweredCaseWord.entrySet()) {
            String loweredCaseWord = entry.getKey()
            List<WordStatusDate> words = entry.getValue()
            if (words.size() > 1) {
                // println "$loweredCaseWord:\n\t${words.join("\n\t")}"

                List<WordStatusDate> allWordsButTheMostCurrent = words.sort().reverse().tail()

                allWordsButTheMostCurrent.forEach(record -> {
                    String word = record.getWord()
                    Path wordRecord = root.resolve("${word}.tsv")
                    boolean deleted = Files.deleteIfExists(wordRecord)
                    if (deleted) {
                        println "Record $wordRecord for word $word was deleted. I just didn't like it :D."
                    }
                })
            }
        }
    }

    private static List<Path> filewalk(Path root) {
        List<Path> result
        try (Stream<Path> walk = Files.walk(root, 1)) {
            result = walk.filter(Files::isRegularFile)
                    .collect(Collectors.toList())
        }
        return result
    }

    private static WordStatusDate readRecord(Path path) {
        List<String> lines = Files.readAllLines(path)
        String firstLine = lines.get(0)

        List<String> splittedLine = firstLine.split("\t").toList()

        String word = splittedLine.get(0)
        String status = splittedLine.get(1)
        String date = splittedLine.get(2)

        return new WordStatusDate(word, status, date)
    }

    static class WordStatusDate implements Comparable<WordStatusDate>
    {
        String word
        String status
        Date date

        static SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        static SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd");

        WordStatusDate(String word, String status, String date) {
            this.word = word
            this.status = status
            this.date = format.parse(date)
        }

        String getWord() {
            return word
        }

        String getStatus() {
            return status
        }

        Date getDate() {
            return date
        }

        Date getDay() {
            Calendar cal = Calendar.getInstance(); // locale-specific
            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            return cal.getTime();
        }

        @Override
        String toString() {
            return "${format.format(date)}\t$word\t$status"
        }

        @Override
        int compareTo(WordStatusDate that) {
            return this.date.compareTo(that.date)
        }
    }

    static void main(String[] args) {
        String wordsPath = "/media/eduard/dades/Baixades/WordsFiles/status/DE/"
        if (args.size() == 1) {
            wordsPath = args[0]
        }
        Path wordsFolder = Paths.get(wordsPath)

        VocabularyCleaner vocabularyCleaner = new VocabularyCleaner(wordsFolder)
        vocabularyCleaner.clean()
    }
}
